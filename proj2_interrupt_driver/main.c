 /***********************************************************
 * 	Driver for Newhaven LCD display using I2C1 bus          *
 * 	For ECE 372 Project 2                                   *
 * 	Created by Martin Rodriguez                             *
 *	                                                        *
 * 	Displays one letter of the name "MARTIN" on LCD         *
 * 	screen for each button press using interrupt-driven     *
 * 	transmission                                            *
 *                                                          *
 ************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Defines section
#define HWREG(x) (*((volatile unsigned int *)(x)))

// GPIO defines
#define GPIO1BA 0x4804C000		// Base address for GPIO1

// INTC defines
#define INTCBA 0x48200000		// Base address for INTC

// I2C defines
#define I2CBA 0x4802A000 		// Base address for I2C1 module
#define I2C_PSC 0xB0	 		// Offset for prescaler
#define I2C_SCLL 0xB4	 		// Offset for SCL tLOW register
#define I2C_SCLH 0xB8	 		// Offset for SCL tHIGH register
#define I2C_OA 0xA8		 		// Offset for own address register
#define I2C_CON 0xA4	 		// Offset for I2C control register
#define I2C_IRQENABLE_SET 0x2C	// Offset for IRQENABLE
#define I2C_SA 0xAC				// Offset for slave address register
#define I2C_CNT 0x98			// Offset for message count (DCOUNT)
#define I2C_IRQSTATUS_RAW 0x24  // Offset for IRQSTATUS_RAW
#define I2C_DATA 0x9C			// Offset for data register
#define I2C_BUF 0x94			// Offset for I2C_BUF
#define I2C_SYSC 0x10			// Offset for I2C_SYSC
#define I2C_SYSS 0x90			// Offset for I2C_SYSS
#define I2C_BUFSTAT	0xC0		// Offset for I2C_BUFSTAT

// Other defines
#define CMBA 0x44E10000			// Base address for control module
#define CLKWKUPS 0x44E00000
#define SLAVE_ADD 0x3C			// 0x78 bitshifted to the right = 0x3C
#define COMSEND 0x00			// Example in datasheet gives 0x00
#define DATASEND 0x40       	// Example in datasheet gives 0x40
#define BBMASK 0x1000			// Mask to check bit 8 (BB) in I2C1_IRQSTATUS_RAW
#define XRDYMASK 0x10			// Mask to check bit 4 (XRDY) in I2C1_IRQSTATUS_RAW
#define ARDYMASK 0x4			// Mask to check bit 2 (ARDY) in I2C1_IRQSTATUS_RAW
#define NACKMASK 0x2			// Mask to check bit 1 (NACK) in I2C1_IRQSTATUS_RAW
#define EXIT_SUCCESS 0

// Function declarations
void IntMasterIRQEnable();
void int_handler();
void wait_loop();
void delay();
void init_LCD();
void button_int();
void send_message();
int poll_BB();
int poll_XRDY();
int poll_ARDY();
int poll_STOP();

// Global variables
int name_text[6] = {0x40, 0x41, 0x52, 0x54, 0x49, 0x4E};
char text[16] = "MARTIN RODRIGUEZ";
int init_codes[10] = {0x00, 0x38, 0x39, 0x14, 0x78, 0x5E, 0x6D, 0x0C, 0x01, 0x06};
int button_count = 0;
// 0x00 = Comsend
// 0x38 = 00111000 > FUNCTION SET 8-bit, N=1, 5x7dot
// 0x39 = 00111001 > FUNCTION SET 8-bit, N=1, 5x7dot, IS=1
// 0x14 = 00010100 > BIAS SET BS=0, FX=0
// 0x78 = 01111000 > CONTRAST SET
// 0x5E = 01011110 > POWER/ICON/CONTRAST SET Ion=1, Bon=1, C[5:4]=1,0
// 0x6D = 01101101 > FOLLOWER CONTROL SET
// 0x0C = 00001100 > DISPLAY ON SET D=1, C=0, B=0
// 0x01 = 00000001 > CLEAR DISPLAY
// 0x06 = 00000110 > ENTRY MODE SET I/D=1, S=0

int BB_flag;
int XRDY_flag;
int NACK_flag;
int ARDY_flag;
int STOP_flag;
volatile unsigned int USR_STACK[1000];
volatile unsigned int INT_STACK[1000];

int main(void) {
	// Set up stacks (unnecessary for polled-mode)
	// init USR stack
	asm("LDR R13, =USR_STACK");
	asm("ADD R13, R13, #0x1000");
	// init IRQ stack
	asm("CPS #0x12");
	asm("LDR R13, =INT_STACK");
	asm("ADD R13, R13, #0x1000");
	asm("CPS #0x13");
	// Just to initialize these to non-zero values. They get changed at the beginning of their respective functions.
	BB_flag = 1;
	XRDY_flag = 1;
	NACK_flag = 1;
	ARDY_flag = 1;
	STOP_flag = 1;
	button_count = 0;

	// GPIO1 INIT
	HWREG(CLKWKUPS + 0xAC) = 0x2; // Turn on clock for GPIO1
	HWREG(GPIO1BA + 0x14C) = 0x40000000; 		// Enable GPIO1 falling detect
	HWREG(GPIO1BA + 0x34) = 0x40000000; 		// Enable GPIO1_30 request on POINTRPEND1

	// I2C1 INIT
	HWREG(CMBA + 0x95C) = 0x32;			// Change P9 pin 17 from conf_spi0_cs0 to I2C1_SCL (MODE2), enable pullup resistor, enable input
	HWREG(CMBA + 0x958) = 0x32;			// Change P9 pin 18 to conf_spi0_d1 to I2C1_SDA (MODE2), enable pullup resistor, enable input

	HWREG(CLKWKUPS + 0x48) = 0x2;		// Turn on clock for I2C1
	HWREG(I2CBA + I2C_SYSC) = 0x0002; 	// Soft reset I2C1
	delay(10000);
	HWREG(I2CBA + I2C_PSC) = 0x3; 		// Set prescaler to obtain 12 MHz clock
	HWREG(I2CBA + I2C_SCLL) = 0x35;		// Set tLOW for 100 kpbs
	HWREG(I2CBA + I2C_SCLH) = 0x37;		// Set tHIGH for 100 kbps

	HWREG(I2CBA + I2C_CON) = 0x8600;	// Configure I2C1 mode register bits (MST, TX = 1)
	HWREG(I2CBA + I2C_SA) = SLAVE_ADD;	// Configure slave address = 0x3C

	// INTC INIT (unnecessary for polled mode)
	HWREG(INTCBA + 0x10) = 0x2;			// reset INTC
	HWREG(INTCBA + 0xC8) = 0x80;		// unmask INTC_I2C1 (INT# 71)
	HWREG(INTCBA + 0xE8) = 0x4;			// unmask INTC_GPIOINT1A (INT# 98)

	// Enable IRQ
	IntMasterIRQEnable();

	// LCD INIT
	init_LCD();

	// Get ready to send data
	poll_XRDY();
	HWREG(I2CBA + I2C_CNT) = 0x17;		// Load number of bytes for transmission
	HWREG(I2CBA + I2C_CON) = 0x8603; 	// Start condition to initiate transfer + stop condition
	// Send data when ready
	HWREG(I2CBA +I2C_DATA) = DATASEND;

	wait_loop();

	return EXIT_SUCCESS;
}


// Function definitions

void wait_loop(void)
{
	while(1)
	{
		// Do nothing loop
	}
}

void int_handler(void)
{
	// asm("STMFD SP!, {R0-R5, LR}");
	if(HWREG(0x482000F8) == 0x00000004) 	// Checking if INTC_PENDING_IRQ3 (INT 98) is set
	{
		button_int(button_count);
	}
	asm("LDMFD SP!, {LR}");
	asm("LDMFD SP!, {LR}");
	asm("SUBS PC, LR, #0x4");
}

void delay(int dly)
{
	int n;
	int doo;
	for(n=0;n<dly;n++)
	{
		doo = doo + 1;
	}
}

void init_LCD(void)
{
	poll_BB();									// poll busy bit
	if(BB_flag == 0)							// If line not busy, begin initialization
	{
		//HWREG(I2CBA + I2C_IRQSTATUS_RAW) = 0x80;	// Reset AERR bit
		HWREG(I2CBA + I2C_CNT) = 0xA;				// Load DCOUNT with number of initialization codes to send (0xA = 10d)
		HWREG(I2CBA + I2C_CON) = 0x8603; 		// Start condition to initiate transfer
		int i;
		for(i=0;i<10;i++)
		{
			poll_XRDY();
			HWREG(I2CBA + I2C_DATA) = init_codes[i];
		}
		delay(10000);
		//poll_ARDY();
		}
}

void button_int(void)
{
	HWREG(GPIO1BA + 0x2C) = 0x40000000;			// Clear GPIO1_30 interrupt
	HWREG(INTCBA + 0x48) = 0x1; 				// Clear NEWIRQ bit in INTC
	// Transmit data via I2C1
	send_message();
	//asm("LDMFD SP!, {LR}");
	//asm("SUBS PC, LR, #0x4");
}

void send_message(void)
{
	poll_XRDY();							// poll XRDY bit in I2C_IRQSTATUS_RAW
	HWREG(I2CBA + I2C_DATA) = text[button_count];
	button_count = button_count + 1;
}

int poll_BB()
{
	while ((BBMASK & HWREG(I2CBA + I2C_IRQSTATUS_RAW)) == 0x1000) // poll busy bit
		{
			BB_flag = 1;	// bus busy
		}
	BB_flag = 0;
	return BB_flag;
}

int poll_XRDY()
 {
	XRDY_flag = 0;
	while ((XRDYMASK & HWREG(I2CBA + I2C_IRQSTATUS_RAW)) == 0x0000) // poll XRDY bit in I2C_IRQSTATUS_RAW
	{
		XRDY_flag = 0;		// XRDY not asserted
	}
	HWREG(I2CBA + I2C_IRQSTATUS_RAW) = XRDYMASK;
	XRDY_flag = 1;
	return XRDY_flag;
}

int poll_ARDY()
{
	ARDY_flag = 0;
	while ((ARDYMASK & HWREG(I2CBA + I2C_IRQSTATUS_RAW)) != 0x4) // poll ARDY bit in I2C_IRQSTATUS_RAW
	{
		ARDY_flag = 0;
		if ((NACKMASK & HWREG(I2CBA + I2C_IRQSTATUS_RAW)) == 0x2) // poll NACK bit in I2C_IRQSTATUS_RAW
		{
			NACK_flag = 1;
			HWREG(I2CBA + I2C_CON) = 0x8602; 		// Stop condition to finish transfer due to NACK
			// I2C communication has failed
			return NACK_flag;
		}
	}
	HWREG(I2CBA + I2C_IRQSTATUS_RAW) = ARDYMASK;
	ARDY_flag = 1;
	return ARDY_flag;
}

int poll_STOP()
{
	STOP_flag = 0;
	while((HWREG(I2CBA + I2C_IRQSTATUS_RAW) & 0x4) == 0x0000)    	// poll stop bit
	{
		STOP_flag = 0;
	}
	STOP_flag = 1;
	return STOP_flag;
}

void IntMasterIRQEnable(void)
{
	asm("MRS R3, CPSR");
	asm("BIC R3, #0x80");
	asm("MSR CPSR_c, R3");
}
